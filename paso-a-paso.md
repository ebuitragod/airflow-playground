
Todo el trabajo en consola que veremos, lo haremos en una consola linux (~terminal/MacOS) suponiendo que tenemos instalado pip y que además cuando escribimos los comandos pip/python estos están en python3, es decir, si al escribir python -V te sale python 2, puedes escribir el comando alias python=python3. Similarmente con pip. 

### =====Repoositorio

>> cd directorio repo
>> git init
>> git remote add origin https://gitlab.com/ebuitragod/airflow-playground.git
>> git remote 
origin
>> git remote -V
origin	https://gitlab.com/ebuitragod/airflow-playground.git (fetch)
origin	https://gitlab.com/ebuitragod/airflow-playground.git (push)
>> git branch -M master
>> git pull origin master


### =====Conda

- Install conda: https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html

- Creemos un ambiente virtual llamado "airflow"
>> conda create --name airflow python=3.7
>> conda activate airflow


### =====Airlow - Installation
- Abre una nueva ventana/pestaña de la terminal
>> conda activate airflow
>> mkdir ~/airflow
>> export AIRFLOW_HOME=~/airflow  
>> echo $AIRFLOW_HOME
>> AIRFLOW_VERSION=2.0.1 
>> echo $AIRFLOW_VERSION
>> PYTHON_VERSION="$(python --version | cut -d " " -f 2 | cut -d "." -f 1-2)"
>> echo $PYTHON_VERSION
>> CONSTRAINT_URL="https://raw.githubusercontent.com/apache/airflow/constraints-${AIRFLOW_VERSION}/constraints-${PYTHON_VERSION}.txt"
>> pip install "apache-airflow==${AIRFLOW_VERSION}" --constraint "${CONSTRAINT_URL}"
>> cd ~/airflow
>> airflow db init
>> airflow users create --username admin --firstname admin --lastname istrator --role Admin --email admin@admin.com
>> vim airflow.cfg #No es necesario por ahora, **lo veremos en el taller**
-- Change the dag's route to the folder where your dags are. 
-- Change the plugins_folder = where the plugging are
>> pip install 'apache-airflow[slack']
>> pip install apache-airflow-backport-providers-mongo
>> pip install 'apache-airflow[rabbitmq]'


### =====Paquetes
- Retornar a la primera ventana
>> ls
Deben aparecer nuestros documentos dentro de nuestro repositorio, en particular requirements.txt
>> pwd
Debe aparecer el directorio de nuestro repositorio
>> pip install -r requirements.txt
>> pip uninstall -y gunicorn 
>> pip install gunicorn==19.5.0
>> pip install --upgrade pip setuptools wheel


### =====Mongo install
- Sigue las instrucciones en la documentación https://docs.mongodb.com/manual/installation/ de acuerdo a tus necesidades
>> Mongo
-Si te abre una consola diferente a la que tienes dentro de tu pestaña de la terminal, estás bien. Para salir has de teclear ctrl/cmnd + c al tiempo


### =====Robot 3T
- Sigue las instrucciones en la documentación https://robomongo.org/download de acuerdo a tus necesidades


### =====Visual Studio Code
- Sigue las instrucciones en la documentación https://visualstudio.microsoft.com/es/downloads/ de acuerdo a tus necesidades


## =============================

#### =====Airflow servidores
##### ===Server
airflow webserver --port 8000

##### ===Scheduler
export AIRFLOW_HOME=~/airflow
#$(pwd)
airflow scheduler


=====