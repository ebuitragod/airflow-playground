from airflow import DAG

from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator

from airflow.operators.python_operator import PythonOperator


from datetime import datetime, timedelta

default_args = {
    'owner': 'espe',
    'depends_on_past': False,
    'start_date': datetime(2021, 11, 1),
    'catchup': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'airflow_playground',
    default_args=default_args,
    schedule_interval='@hourly',
    tags=['python', 'pyladies', 'pybcn']
    
)

start_button_dag = DummyOperator(
    task_id='button',
    dag=dag
)

bash_dag = BashOperator(
    task_id='espe_bash',
    bash_command="echo 'hola a todos'",
    dag=dag
)

def hola(x, y):
    print(f'{x} says hi to {y}')
    return True

python_dag = PythonOperator(
    task_id = 'python_dag',
    python_callable=hola,
    op_kwargs={
        'x': 'Gracias',
        'y': 'ambos!'
    },
    dag=dag
)
start_button_dag >> [bash_dag, python_dag]